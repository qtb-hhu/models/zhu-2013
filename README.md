# Zhu 2013

Implementation of model published [here](https://doi.org/10.1111/pce.12025).

## Current status

**This model is not fully implemented**

## Setting up

- `pip install pre-commit`
- `pre-commit install`
- `pip install -r code/requirements.txt`
